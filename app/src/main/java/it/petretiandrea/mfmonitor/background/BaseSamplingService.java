package it.petretiandrea.mfmonitor.background;

import android.app.Service;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.background
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 21/11/17.
 */

// todo: fix thread safe.
public abstract class BaseSamplingService extends Service {

    private static final String TAG = BaseSamplingService.class.getName();

    /**
     * Callback interface.
     */
    private ServiceMonitorCallback _monitorCallback;

    /**
     * Handler thread for async execution of monitor
     */
    private HandlerThread _handlerThread;

    /**
     * Handler for post runnable to Handler thread.
     */
    private Handler _handler;


    /**
     * Total record time
     */
    private int _recordTime;

    /**
     * If the handler thread is running.
     */
    private boolean _isRunning;


    // TODO: INSTANZIARE OGGETTO.
    private SamplingTask _samplingTask;

    /**
     * Lock for running status, sample time
     */
    private ReentrantLock _lock;

    @Override
    public void onCreate() {
        super.onCreate();

        _lock = new ReentrantLock();
        _isRunning = false;

        _recordTime = -1;
    }

    public Handler getHandler()
    {
        _lock.lock();
        try {
            return _handler;
        } finally {
            _lock.unlock();
        }
    }

    /**
     * Start the handler thread for monitoring the sensor
     * @param samplingTime The sampling time or waiting time.
     */
    protected void startHandlerThread(int samplingTime, int recordTime)
    {
        Log.d(TAG, "Starting monitor thread");
        _lock.lock();
        try
        {
            _recordTime = recordTime;
            if(_handlerThread == null)
                _handlerThread = new HandlerThread("SamplingThread");

            _handlerThread.start();
            _handler = new Handler(_handlerThread.getLooper());

            // Task to be executed, first the sampling task, after task for stop the thread.
            _samplingTask = new SamplingTask(this);

            _handler.post(_samplingTask);
            _handler.post(this::stopHandlerThread);

            _isRunning = true;
            Log.d(TAG, "Started monitor thread");
        } finally {
            _lock.unlock();
        }

        if(_monitorCallback != null)
            _monitorCallback.onStartMonitoring();
    }

    /**
     * Stop and quit from handler thread.
     */
    protected void stopHandlerThread()
    {
        Log.d(TAG, "Stopping monitor thread");
        _lock.lock();
        try
        {
            _handler.removeCallbacks(_samplingTask);
            _handler.removeCallbacks(this::stopHandlerThread);
            _handlerThread.quit();
            _handlerThread = null;
            _isRunning = false;
            Log.d(TAG, "Stopped monitor thread");
        } finally {
            _lock.unlock();
        }

        if(_monitorCallback != null)
            _monitorCallback.onStopMonitoring();
    }

    /**
     * Set a callback for monitor, or handler thread status.
     * @param callback
     */
    public void setMonitorListener(ServiceMonitorCallback callback)
    {
        _monitorCallback = callback;
    }

    /**
     * If handler thread is running
     * @return true if handler thread is running.
     */
    public boolean isRunning()
    {
        try {
            _lock.lock();
            return _isRunning;
        } finally {
            _lock.unlock();
        }
    }
}
