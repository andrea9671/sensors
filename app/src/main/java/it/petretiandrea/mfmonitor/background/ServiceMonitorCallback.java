package it.petretiandrea.mfmonitor.background;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.background
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 22/11/17.
 */

public interface ServiceMonitorCallback {

    void onStartMonitoring();
    void onStopMonitoring();

}
