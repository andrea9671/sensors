package it.petretiandrea.mfmonitor.background;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import it.petretiandrea.mfmonitor.R;
import it.petretiandrea.mfmonitor.utils.Constants;
import it.petretiandrea.mfmonitor.utils.NotificationHelper;

/**
 * Project Sensors
 * Created by Petreti Andrea petretiandrea@gmail.com
 * Created at 17/11/17.
 */

// TODO: Considereare l'utilizzo di IntentService.
// TODO: Aggiungere salvataggio stato tramite le preferences!
public class ForegroundSamplingService extends BaseSamplingService {

    private static final String TAG = ForegroundSamplingService.class.getName();

    private IBinder _binder = new LocalBinder();

    /**
     * Notification for foreground execution.
     */
    private Notification _notification;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return _binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate!");

        _notification = new NotificationHelper(this)
                .getNotificationOne(getString(R.string.app_name), getString(R.string.notification_body_background))
                .setSmallIcon(R.drawable.ic_notification)
                .build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand!");

        if(intent.getStringExtra(Constants.FOREGROUND_SERVICE_START).equalsIgnoreCase(Constants.FOREGROUND_SERVICE_START))
        {
            // in fase di avvio recupero anche il sampling time
            // Start foreground and thread
            int samplingTime = intent.getIntExtra(Constants.FOREGROUND_SAMPLING_TIME, -1);
            int recordTime = intent.getIntExtra(Constants.FOREGROUND_RECORD_TIME, -1);

            // todo: instanziare la lista in oggetti concreti
            String[] sensorList = intent.getStringArrayExtra(Constants.FOREGROUND_SENSOR_LIST);
            //setMonitorList(...);

            //if(samplingTime > -1 && recordTime > -1)
            startHandlerThread(samplingTime, recordTime);
            //else
            //    Log.d(TAG, "Invalid sampling time or record time");
        }
        else if(intent.getStringExtra(Constants.FOREGROUND_SERVICE_STOP).equalsIgnoreCase(Constants.FOREGROUND_SERVICE_STOP))
        {
            // Stop foreground and thread
            stopHandlerThread();
        }

        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy!");
    }

    @Override
    protected void startHandlerThread(int samplingTime, int recordTime) {
        super.startHandlerThread(samplingTime, recordTime);
        startForeground(Constants.FOREGROUND_SERVICE_ID, _notification);
    }

    @Override
    protected void stopHandlerThread() {
        super.stopHandlerThread();
        stopForeground(true);
        stopSelf();
    }

    public class LocalBinder extends Binder
    {
        public ForegroundSamplingService getService()
        {
            return ForegroundSamplingService.this;
        }
    }
}
