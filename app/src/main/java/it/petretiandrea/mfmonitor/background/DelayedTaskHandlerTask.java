package it.petretiandrea.mfmonitor.background;


import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

/**
 * Created by andrea on 07/05/18.
 */

public class DelayedTaskHandlerTask {

    public interface Observer {
        void onStartCountdown();
        void onTick(long remeaningTime);
        void onCountdownEnd();
    }

    private long mDelay;
    private Handler mHandler;

    private boolean mStarted;
    private static final int GRANULARITY_STEP = 1000;
    private static final int COUNTDOWN_START = 1010;
    private static final int COUNTDOWN_END = 1020;
    private int mSteps;

    private Observer mObserver;

    public long getDelay() {
        return mDelay;
    }

    public void setObserver(Observer observer) {
        mObserver = observer;
    }

    public DelayedTaskHandlerTask(long delay) {
        mHandler = new Handler(msg -> {
            if(msg.arg1 == COUNTDOWN_START && mObserver != null)
                mObserver.onStartCountdown();
            else if(msg.arg1 == COUNTDOWN_END && mObserver != null)
                mObserver.onCountdownEnd();
            else if(msg.obj != null && mObserver != null)
                mObserver.onTick((Integer) msg.obj);
            else
                return false;
            return true;
        });

        mDelay = delay;
        mStarted = false;
        mSteps = (int) (delay / GRANULARITY_STEP);
    }

    public void start() {
        mStarted = true;
        Message msg = new Message();
        msg.arg1 = COUNTDOWN_START;
        mHandler.sendMessage(msg);
        mHandler.post(task);
    }

    public void cancel() {
        mStarted = false;
        mHandler.removeCallbacks(task);
    }

    private Runnable task = new Runnable() {
        @Override
        public void run() {
            if(mStarted) {
                if(mSteps > 0) {
                    System.out.println("T: " + (mSteps * GRANULARITY_STEP));
                    Message msg = new Message();
                    msg.obj = (mSteps * GRANULARITY_STEP);
                    mHandler.sendMessage(msg);
                    mHandler.postDelayed(this, GRANULARITY_STEP);
                    mSteps--;
                } else {
                    // time elapsed
                    Message msg = new Message();
                    msg.arg1 = COUNTDOWN_END;
                    mHandler.sendMessage(msg);
                }
            }
        }
    };
}
