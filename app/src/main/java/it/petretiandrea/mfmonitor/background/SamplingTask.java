package it.petretiandrea.mfmonitor.background;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;

import com.google.android.gms.tasks.Tasks;
import com.google.firebase.crash.FirebaseCrash;

import java.util.concurrent.ExecutionException;

import it.petretiandrea.mfmonitor.model.FileCSVManager;
import it.petretiandrea.mfmonitor.model.FirebaseLogManager;
import it.petretiandrea.mfmonitor.model.WakeUp;
import it.petretiandrea.mfmonitor.model.monitor.GPSMonitor;
import it.petretiandrea.mfmonitor.model.monitor.MagnetometerMonitor;
import it.petretiandrea.mfmonitor.model.monitor.NetworkMonitor;
import it.petretiandrea.mfmonitor.model.monitor.PermissionException;
import it.petretiandrea.mfmonitor.model.monitor.WifiMonitor;
import it.petretiandrea.mfmonitor.model.monitor.record.LocationRecord;
import it.petretiandrea.mfmonitor.model.monitor.record.LogResult;
import it.petretiandrea.mfmonitor.model.monitor.record.WifiResult;
import it.petretiandrea.mfmonitor.utils.Average;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.background
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 15/02/18.
 */

public class SamplingTask extends ContextWrapper implements Runnable {

    private static final String TAG = SamplingTask.class.getName();

    private static final int AVERAGE_SAMPLE_NUMBER = 10;
    private final FileCSVManager _csvManager;

    /** Monitor for magnetometer **/
    private MagnetometerMonitor _magnetometerMonitor;

    /** Monitor for wifi **/
    private WifiMonitor _wifiMonitor;

    /** Monitor for GPS **/
    private GPSMonitor _gpsMonitor;

    /** Network monitor for Mobile **/
    private NetworkMonitor _networkMonitor;

    /** WakeUp manager for CPU **/
    private WakeUp _wakeUp;

    public SamplingTask(Context base)
    {
        super(base);
        _magnetometerMonitor = new MagnetometerMonitor(this, 3, 15000);
        _wifiMonitor = new WifiMonitor(this);
        _gpsMonitor = new GPSMonitor(this);
        _networkMonitor = new NetworkMonitor(this);
        _csvManager = new FileCSVManager(this);
        _wakeUp = new WakeUp(this);
    }

    @Override
    public void run() {
        LogResult logResult = new LogResult();

        /* Acquire lock for CPU */
        _wakeUp.wakeUp(10 * 60 * 1000); // timeout of 10 minutes if is not relase before, release it automatically



        /// Misurazione campo magnetico.
        try {
            double module = Average.average(Average.averageEvery(Tasks.await(_magnetometerMonitor.startSampling()), AVERAGE_SAMPLE_NUMBER));
            logResult.setMagnetometer(module);
            FirebaseCrash.log("Magnetic Field: " + module);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        /// END

        /// Misurazione numero di access point
        try {
            WifiResult aps = Tasks.await(_wifiMonitor.startScan());
            logResult.setWifiResult(aps);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        /// END

        /// Misurazione posizione utente
        try {
            LocationRecord userLocation = Tasks.await(_gpsMonitor.getBestUserLocation());
            logResult.setLocationResult(userLocation);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        } catch (PermissionException e) {
            Log.w(TAG, "No location permission!");
        }
        /// END

       logResult.setMobileData(_networkMonitor.isMobileActive());

        _csvManager.save(logResult);

        FirebaseLogManager.getInstance().saveLog(logResult);

        /* Release lock */
        _wakeUp.release();
    }
}
