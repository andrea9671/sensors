package it.petretiandrea.mfmonitor.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import it.petretiandrea.mfmonitor.utils.PreferencesHelper;
import it.petretiandrea.mfmonitor.utils.SensorMonitorHelper;


/**
 * Project Sensors
 * Package it.petretiandrea.sensors
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 21/11/17.
 */

/**
 * Broadcast receiver for device boot message
 */
public class BootBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = BootBroadcastReceiver.class.getName();
    private static final int DELAY = 3 * (1000 * 60); // 3 minutes

    @Override
    public void onReceive(Context context, Intent intent) {
        // Start the allarm manager
        Log.d(TAG, "Boot received!");
        PreferencesHelper preferencesHelper = new PreferencesHelper(context);
        if(preferencesHelper.isAlarmMonitorScheduled())
        {
            SensorMonitorHelper.getInstance().schedule(context, DELAY);
        }
    }
}