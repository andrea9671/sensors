package it.petretiandrea.mfmonitor.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import it.petretiandrea.mfmonitor.background.ForegroundSamplingService;
import it.petretiandrea.mfmonitor.utils.Constants;
import it.petretiandrea.mfmonitor.utils.SensorMonitorHelper;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.broadcast
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 21/11/17.
 */

/**
 * Class receiver for start the foreground service that register sensors data.
 */
public class AlarmBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = AlarmBroadcastReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        intent.setClass(context, ForegroundSamplingService.class);

        Log.d(TAG, "Alarm received, starting foreground service");
        // getting next delay
        int nextDelay = intent.getIntExtra(Constants.NEXT_SCHEDULE_INTERVAL, -1);
        Log.d(TAG, "Next run to " + nextDelay + "ms");
        SensorMonitorHelper.getInstance().schedule(context, nextDelay);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            context.startForegroundService(intent);
        else
            context.startService(intent);
    }
}
