package it.petretiandrea.mfmonitor.preferences;

import android.content.Context;
import android.preference.MultiSelectListPreference;
import android.util.AttributeSet;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.preferences
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 16/12/17.
 */

public class SensorMultiSelectPreference extends MultiSelectListPreference {

    public SensorMultiSelectPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public SensorMultiSelectPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SensorMultiSelectPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SensorMultiSelectPreference(Context context) {
        super(context);
    }


}
