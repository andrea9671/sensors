package it.petretiandrea.mfmonitor.preferences;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.preferences
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 15/12/17.
 */

public class SampleIntervalDialog extends DialogPreference {

    public SampleIntervalDialog(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public SampleIntervalDialog(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SampleIntervalDialog(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SampleIntervalDialog(Context context) {
        super(context);
    }



}
