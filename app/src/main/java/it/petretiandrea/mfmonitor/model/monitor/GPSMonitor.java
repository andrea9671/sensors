package it.petretiandrea.mfmonitor.model.monitor;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.HandlerThread;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import it.petretiandrea.mfmonitor.model.monitor.record.LocationRecord;

import static android.location.LocationManager.*;

/**
 * Created by andrea on 13/02/18.
 */

public class GPSMonitor extends ContextWrapper implements LocationListener {

    private static final String TAG = GPSMonitor.class.getName();

    private static final int USER_LOCATION_TIMEOUT = 20000; // 20 SECONDS
    private static final int INDOOR_THRESHOLD_ACCURACY_NET = 10; // over 10 is indoor with net
    private static final int INDOOR_THRESHOLD_ACCURACY_GPS = 600; // over 600 is indoor with gps

    private Location _lastGpsLocation;
    private Location _lastNetLocation;

    /** Lock for guard _lastLocation **/
    private ReentrantLock _lock;

    private Semaphore _semaphore;

    public GPSMonitor(Context base) {
        super(base);
        _lock = new ReentrantLock();
        _semaphore = new Semaphore(0);
    }

    public Task<LocationRecord> getBestUserLocation() throws PermissionException {

        return Tasks.call(Executors.newSingleThreadExecutor(), () -> {
            if(checkPermission())
            {
                LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                int providerCount = 0;
                if(locationManager != null)
                {
                    HandlerThread handlerThread = new HandlerThread("GPSMonitorThread");
                    handlerThread.start();
                    if(locationManager.isProviderEnabled(GPS_PROVIDER)) {
                        locationManager.requestSingleUpdate(GPS_PROVIDER, GPSMonitor.this, handlerThread.getLooper());
                        providerCount++;
                    }
                    if(locationManager.isProviderEnabled(NETWORK_PROVIDER)) {
                        locationManager.requestSingleUpdate(NETWORK_PROVIDER, GPSMonitor.this, handlerThread.getLooper());
                        providerCount++;
                    }

                    try {
                        _semaphore.tryAcquire(providerCount, USER_LOCATION_TIMEOUT, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    locationManager.removeUpdates(GPSMonitor.this);
                    handlerThread.quit();

                    // TODO: Use isBetterLocation from oldLocation.
                    if(getLastGpsLocation() != null)
                        return new LocationRecord(getLastGpsLocation(), isIndoor(getLastGpsLocation(), GPS_PROVIDER));
                    if(getLastNetLocation() != null)
                        return new LocationRecord(getLastNetLocation(), isIndoor(getLastNetLocation(), NETWORK_PROVIDER));
                }
            } else throw new PermissionException("No location permission!");

            return new LocationRecord(null, true);
        });
    }

    private boolean checkPermission()
    {
        int perm1 = this.checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        int perm2 = this.checkCallingOrSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
        return (perm1 == PackageManager.PERMISSION_GRANTED && perm2 == PackageManager.PERMISSION_GRANTED);
    }

    public Location getLastGpsLocation()
    {
        _lock.lock();
        try {
            return _lastGpsLocation;
        } finally {
            _lock.unlock();
        }
    }

    public Location getLastNetLocation()
    {
        _lock.lock();
        try {
            return _lastNetLocation;
        } finally {
            _lock.unlock();
        }
    }

    public void setLastGpsLocation(Location location)
    {
        _lock.lock();
        try {
            _lastGpsLocation = location;
        } finally {
            _lock.unlock();
        }
    }

    public void setLastNetLocation(Location location)
    {
        _lock.lock();
        try {
            _lastNetLocation = location;
        } finally {
            _lock.unlock();
        }
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {

        int TWO_MINUTES = 1000 * 60 * 2;

        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    public boolean isIndoor(Location location, String provider) {
        if(location != null) {
            if(provider.equals(NETWORK_PROVIDER))
                return location.getAccuracy() >= INDOOR_THRESHOLD_ACCURACY_NET;
            else if(provider.equals(GPS_PROVIDER))
                return location.getAccuracy() >= INDOOR_THRESHOLD_ACCURACY_GPS;
        }
        return false;
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location.getProvider().equals(NETWORK_PROVIDER)) {
            setLastNetLocation(location);
            _semaphore.release();
        }
        else if(location.getProvider().equals(GPS_PROVIDER)) {
            setLastGpsLocation(location);
            _semaphore.release();
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}