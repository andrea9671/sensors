package it.petretiandrea.mfmonitor.model;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.PowerManager;

/**
 * Created by andrea on 26/04/18.
 */

public class WakeUp extends ContextWrapper {

    private static final String TAG = WakeUp.class.getName();
    private PowerManager.WakeLock mWakeLock;

    public WakeUp(Context base) {
        super(base);
        PowerManager pwm = (PowerManager) getSystemService(POWER_SERVICE);
        assert pwm != null;
        mWakeLock = pwm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, TAG);
    }

    public void wakeUp(long timeout) {
        mWakeLock.acquire(timeout);
    }

    public void release() {
        mWakeLock.release(PowerManager.RELEASE_FLAG_WAIT_FOR_NO_PROXIMITY);
    }


}
