package it.petretiandrea.mfmonitor.model.monitor.record;

import java.io.Serializable;

import it.petretiandrea.mfmonitor.model.CSV;
import it.petretiandrea.mfmonitor.utils.Utility;

/**
 * Created by Andrea on 20/02/2018.
 */

public class LogResult implements CSV, Serializable{

    private double _magnetometer;
    private WifiResult _wifiResult;
    private LocationRecord _locationResult;
    private boolean _mobileData;

    private String _date;

    public LogResult() {
        _date = Utility.getCurrentDateTime();
    }

    public void setMagnetometer(double value)
    {
        _magnetometer = value;
    }

    public void setWifiResult(WifiResult wifiResult)
    {
        _wifiResult = wifiResult;
    }

    public void setLocationResult(LocationRecord locationResult)
    {
        _locationResult = locationResult;
    }

    public void setMobileData(boolean mobileData)
    {
        _mobileData = mobileData;
    }

    public double getMagnetometer() {
        return _magnetometer;
    }

    public WifiResult getWifiResult() {
        return _wifiResult;
    }

    public LocationRecord getLocationResult() {
        return _locationResult;
    }

    public boolean isMobileData() {
        return _mobileData;
    }

    public String getDate() {
        return _date;
    }

    @Override
    public String toCSVHeader() {
        return "date" + "," +
                "magnetometer" + "," +
                _wifiResult.toCSVHeader() + "," +
                "mobile_data" + "," +
                _locationResult.toCSVHeader();
    }

    @Override
    public String toCSVContent() {
        return _date + "," +
                Double.toString(_magnetometer) + "," +
                _wifiResult.toCSVContent() + "," +
                _mobileData + "," +
                _locationResult.toCSVContent();
    }

    public static LogResult parseFromCsv(String csv)
    {
        LogResult logResult = new LogResult();
        String[] line = csv.split("\n");
        if(line.length > 1) {
            String[] headers = line[0].split(",");
            String[] content = line[1].split(",");
            for(int i = 0; i < headers.length; i++)
            {
                if(headers[i].equals("magnetometer"))
                    logResult.setMagnetometer(Double.parseDouble(content[i]));
                else if(headers[i].equals("mobile_data"))
                    logResult.setMobileData(Boolean.parseBoolean(content[i]));
                else if (headers[i].equals("date")) {
                    logResult._date = content[i];
                }
            }
        }

        // for parse wifiresult and locationresult we delegate to him to parse csv
        logResult.setLocationResult(LocationRecord.parseFromCsv(csv));
        logResult.setWifiResult(WifiResult.parseFromCsv(csv));
        return logResult;
    }
}
