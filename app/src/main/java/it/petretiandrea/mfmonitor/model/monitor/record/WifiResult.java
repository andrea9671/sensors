package it.petretiandrea.mfmonitor.model.monitor.record;

import android.net.wifi.ScanResult;
import android.support.v4.util.Pair;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.petretiandrea.mfmonitor.model.CSV;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.model
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 15/02/18.
 */

public class WifiResult implements CSV, Serializable {

    private int _numberAccessPoint;
    private List<Pair<String, Integer>> _bssidRssi;

    private WifiResult() {
        _bssidRssi = new ArrayList<>();
    }

    public WifiResult(List<ScanResult> scanResults)
    {
        _numberAccessPoint = scanResults.size();
        _bssidRssi = new ArrayList<>(scanResults.size());

        for(ScanResult s : scanResults)
            _bssidRssi.add(new Pair<>(s.BSSID, s.level));

    }

    public int getNumberAccessPoint() {
        return _numberAccessPoint;
    }

    public List<Pair<String, Integer>> getBssidRssi() {
        return _bssidRssi;
    }

    @Override
    public String toString() {
        return "WifiResult{" +
                "_numberAccessPoint=" + _numberAccessPoint +
                ", _bssidRssi=" + _bssidRssi +
                '}';
    }

    @Override
    public String toCSVHeader() {
        return "wifi_ap,numberAccessPoint";
    }

    @Override
    public String toCSVContent() {
        StringBuilder builder = new StringBuilder();
        builder.append("\"[");
        int i = 0;
        while (i < this._bssidRssi.size()) {
            builder.append(this._bssidRssi.get(i).first)
                    .append(";")
                    .append(this._bssidRssi.get(i).second)
                    .append(i == this._bssidRssi.size() + -1 ? "" : ",");
            i++;
        }
        builder.append("]\"");
        builder.append(",");
        builder.append(this._numberAccessPoint);
        return builder.toString();
    }

    public static WifiResult parseFromCsv(String csv)
    {
        WifiResult wifiResult = new WifiResult();
        String[] line = csv.split("\n");
        if (line.length > 1) {
            String[] headers = line[0].split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
            String[] content = line[1].split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
            for (int i = 0; i < headers.length; i++) {
                if (headers[i].equals("numberAccessPoint")) {
                    wifiResult._numberAccessPoint = Integer.parseInt(content[i]);
                } else if (headers[i].equals("wifi_ap")) {
                    for (String ap : content[i].replace("[", "").replace("]", "").split(",")) {
                        Matcher matcher = Pattern.compile("(?<bssid>.{2}:.{2}:.{2}:.{2}:.{2}:.{2});(?<rssi>-\\d+)").matcher(ap);
                        if (matcher.find()) {
                            wifiResult._bssidRssi.add(new Pair(matcher.group(1), Integer.parseInt(matcher.group(2))));
                        }
                    }
                }
            }
        }
        return wifiResult;
    }
}
