package it.petretiandrea.mfmonitor.model;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.concurrent.Callable;
import java.util.function.Function;

import it.petretiandrea.mfmonitor.model.monitor.record.LogResult;
import it.petretiandrea.mfmonitor.utils.Utility;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.model
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 15/02/18.
 */

// TODO: AGGIUNGERE AUTENTICAZIONE
public class FirebaseLogManager {

    private static final String TAG = FirebaseLogManager.class.getName();

    private static final String DATA_PATH = "/dati/";
    private FirebaseDatabase _firebaseDatabase;
    private FirebaseAuth _auth;

    private static FirebaseLogManager _instance;

    public static FirebaseLogManager getInstance()
    {
        if(_instance == null)
            _instance = new FirebaseLogManager();
        return _instance;
    }

    private FirebaseLogManager()
    {
        _firebaseDatabase = FirebaseDatabase.getInstance();
        _auth = FirebaseAuth.getInstance();
    }

    private void authenticate(Callable<Void> saveFunction) {
        _auth.signInWithEmailAndPassword("mfmonitor@gmail.com", "MFMonitor96").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {
                    Log.d(TAG, "Firebase User sign in success!");
                    if(_auth.getCurrentUser() != null) {
                        try {
                            saveFunction.call();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else Log.d(TAG, "Firebase user not logged!");
            }
        });
    }

    public boolean saveLog(LogResult logResult)
    {
        authenticate(() -> {
            DatabaseReference ref = _firebaseDatabase.getReference();
            ref.child(DATA_PATH)
                    .child(Utility.getDeviceName())
                    .push()
                    .setValue(logResult)
                    .addOnCompleteListener(task -> Log.i(TAG, "Upload on firebase complete"));
            return null;
        });
        return true;
    }
}
