package it.petretiandrea.mfmonitor.model;

/**
 * Created by Andrea on 19/02/2018.
 */

public interface CSV {

    String toCSVHeader();
    String toCSVContent();
}
