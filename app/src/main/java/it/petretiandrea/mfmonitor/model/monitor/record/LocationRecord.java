package it.petretiandrea.mfmonitor.model.monitor.record;

import android.location.Location;

import java.io.Serializable;
import java.util.Locale;

import it.petretiandrea.mfmonitor.model.CSV;


/**
 * Created by andrea on 13/02/18.
 */

/***
 * Rappresenta il record riguardante la posizione dell'utente.
 */
public class LocationRecord implements CSV, Serializable {

    private boolean _locationAvailable;
    private double _latitude;
    private double _longitude;
    private float _accuracy;
    private boolean _inDoor;
    private String _provider;

    public LocationRecord() { }

    public LocationRecord(Location location, boolean inDoor) {
        _locationAvailable = (location != null);
        if(_locationAvailable) {
            _accuracy = location.getAccuracy();
            _latitude = location.getLatitude();
            _longitude = location.getLongitude();
            _provider = location.getProvider();
        }
        _inDoor = inDoor;
    }

    public double getLatitude() {
        return _latitude;
    }

    public double getLongitude() {
        return _longitude;
    }

    public float getAccuracy() {
        return _accuracy;
    }

    public boolean isInDoor() {
        return _inDoor;
    }

    public String getProvider() {
        return _provider;
    }

    public boolean isLocationAvailable() {
        return _locationAvailable;
    }

    @Override
    public String toString() {
        return "LocationRecord{" +
                "_locationAvailable=" + _locationAvailable +
                ", _latitude=" + _latitude +
                ", _longitude=" + _longitude +
                ", _accuracy=" + _accuracy +
                ", _inDoor=" + _inDoor +
                ", _provider='" + _provider + '\'' +
                '}';
    }

    @Override
    public String toCSVHeader() {
        return "locationAvailable,latitude,longitude,accuracy,indoor,provider";
    }

    @Override
    public String toCSVContent() {
        return String.format(Locale.US, "%b,%f,%f,%f,%b,%s",
                _locationAvailable,
                _latitude,
                _longitude,
                _accuracy,
                _inDoor,
                _provider);
    }

    public static LocationRecord parseFromCsv(String csv)
    {
        LocationRecord locationRecord = new LocationRecord();
        LogResult logResult = new LogResult();
        String[] line = csv.split("\n");
        if(line.length > 1) {
            String[] headers = line[0].split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
            String[] content = line[1].split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
            for(int i = 0; i < headers.length; i++)
            {
                switch (headers[i])
                {
                    case "locationAvailable":
                        locationRecord._locationAvailable = Boolean.parseBoolean(content[i]);
                        break;
                    case "latitude":
                        locationRecord._latitude = Double.parseDouble(content[i]);
                        break;
                    case "longitude":
                        locationRecord._longitude = Double.parseDouble(content[i]);
                        break;
                    case "accuracy":
                        locationRecord._accuracy = Float.parseFloat(content[i]);
                        break;
                    case "indoor":
                        locationRecord._inDoor = Boolean.parseBoolean(content[i]);
                        break;
                    case "provider":
                        locationRecord._provider = content[i];
                        break;
                }
            }
        }
        return locationRecord;
    }
}
