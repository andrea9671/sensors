package it.petretiandrea.mfmonitor.model;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import it.petretiandrea.mfmonitor.utils.Constants;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.model
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 15/02/18.
 */

public class FileLogManager extends ContextWrapper
{
    public enum DateFormatter {

        DEFAULT("dd-MM-yyyy HH:mm:ss:SSS", Locale.US),
        FILELOGMANAGER("yyyy_MM_dd_HH_mm_ss_SSS", Locale.US);

        private SimpleDateFormat _format;
        DateFormatter(String pattern, Locale locale) {
            _format = new SimpleDateFormat(pattern, locale);
        }

        public String format(Date date) {
            return _format.format(date);
        }
        public Date parse(String date) throws ParseException {
            return _format.parse(date);
        }
    }

    private static final String NAME_FILE = "log";
    private static final String EXTENSION_FILE = ".csv";
    private static final String TAG = FileLogManager.class.getName();

    private File _rootDir;

    public FileLogManager(Context base)
    {
        super(base);
        _rootDir = new File(Environment.getExternalStorageDirectory(), Constants.SENSOR_DIR_NAME);
        createDirs();
    }

    private void createDirs()
    {
        if(!_rootDir.exists()) {
            _rootDir.mkdirs();
            Log.d(TAG, "Directory created!");
        }
    }

    public boolean saveCsv(CSV csv)
    {
        String fileName = NAME_FILE + EXTENSION_FILE;
        OutputStream outputStream = null;
        try {
            if (isExternalStorageWritable()) {
                Log.d(TAG, "External storage");
                outputStream = openExternalOutputstream(fileName);
            } else {
                Log.d(TAG, "Interal storage");
                outputStream = openFileOutput(fileName, MODE_APPEND);
            }



        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            outputStream.write(csv.toCSVHeader().concat("\n").getBytes());
            outputStream.write(csv.toCSVContent().getBytes());
            outputStream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String readContent(String filename)
    {
        InputStream inputStream = null;
        try {
            if (isExternalStorageWritable()) {
                Log.d(TAG, "External storage");
                inputStream = openExternalInputStream(filename);
            } else {
                Log.d(TAG, "Interal storage");
                inputStream = openFileInput(filename);
            }
            return new Scanner(inputStream).useDelimiter("\\A").next();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public int getNumberSavedLog() {
        return _rootDir.listFiles((dir, name) -> name.toLowerCase().endsWith(EXTENSION_FILE)).length;
    }

    public File[] getSavedLogName() {
        return _rootDir.listFiles((dir, name) -> name.toLowerCase().endsWith(EXTENSION_FILE));
    }

    public FileInputStream openExternalInputStream(String filename) throws FileNotFoundException {
        File file = new File(_rootDir, filename);
        return new FileInputStream(file);
    }

    public FileOutputStream openExternalOutputstream(String filename) throws IOException
    {
        File file = new File(_rootDir, filename);
        if(!file.exists())
            file.createNewFile();
        return new FileOutputStream(file, true);
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
}
