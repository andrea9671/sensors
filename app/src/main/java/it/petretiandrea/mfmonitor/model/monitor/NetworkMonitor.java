package it.petretiandrea.mfmonitor.model.monitor;

import android.content.Context;
import android.content.ContextWrapper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.model.monitor
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 15/02/18.
 */

public class NetworkMonitor extends ContextWrapper {

    private ConnectivityManager _connectivityManager;

    public NetworkMonitor(Context base) {
        super(base);
        _connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public boolean isMobileActive()
    {
        NetworkInfo.State state =  _connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();
        return (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING);
    }

}
