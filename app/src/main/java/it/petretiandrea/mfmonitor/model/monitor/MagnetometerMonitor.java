package it.petretiandrea.mfmonitor.model.monitor;

import android.content.Context;
import android.content.ContextWrapper;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by andrea on 02/11/17.
 */

public class MagnetometerMonitor extends ContextWrapper implements SensorEventListener{

    private static final String TAG = MagnetometerMonitor.class.getName();

    // Android sensor class
    private SensorManager _sensorManager;
    private Sensor _sensor;

    private int _samplingPeriodUs;
    private int _recordTimeMs;

    private List<Double> _samples;

    // Thread for onSensorChangedEvent.
    private HandlerThread _handlerThread;

    public MagnetometerMonitor(Context base)
    {
        super(base);
        initSensorManager();
        initSensor();
    }

    public MagnetometerMonitor(Context base, int samplingPeriodUs, int recordTimeMs) {
        super(base);
        initSensorManager();
        initSensor();
        _samplingPeriodUs = samplingPeriodUs;
        _recordTimeMs = recordTimeMs;
    }

    private void initSensorManager()
    {
        _sensorManager = (SensorManager) getApplicationContext().getSystemService(Context.SENSOR_SERVICE);
    }

    private void initSensor()
    {
        _sensor = _sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    private void register() {
        if(_sensor != null) {
            _handlerThread = new HandlerThread("MagnetometerThread");
            _handlerThread.start();
            _sensorManager.registerListener(this, _sensor, _samplingPeriodUs, _samplingPeriodUs, new Handler(_handlerThread.getLooper()));
        }
    }

    private void unregister()
    {
        if(_sensor != null)
        {
            _sensorManager.unregisterListener(this, _sensor);
            _handlerThread.quit();
        }
    }

    public Task<Double[]> startSampling()
    {
        return startSampling(_samplingPeriodUs, _recordTimeMs);
    }

    public Task<Double[]> startSampling(int samplingPeriodUs, int recordTimeMs)
    {
        int baseDim = (int) (recordTimeMs / (samplingPeriodUs / 1000d)); // dimensione base array.
        _samples = new ArrayList<>(baseDim + 2);
        return Tasks.call(Executors.newSingleThreadExecutor(), () -> {
            register();
            Thread.sleep(_recordTimeMs);
            unregister();
            return _samples.toArray(new Double[_samples.size()]);
        });
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
        {
            // Lettura componenti vettoriali
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            // Calcolo modulo del vettore e lo aggiundo ai campioni
            _samples.add(Math.sqrt((x * x) + (y * y) + (z * z)));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        Log.d(TAG, "Accuracy changed for " + sensor.getName() + " to: " + i);
    }
}
