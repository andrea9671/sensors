package it.petretiandrea.mfmonitor.model.monitor;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.model.monitor
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 15/02/18.
 */

public class PermissionException extends Exception {
    public PermissionException(String message) {
        super(message);
    }
}
