package it.petretiandrea.mfmonitor.model;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import it.petretiandrea.mfmonitor.model.monitor.record.LogResult;
import it.petretiandrea.mfmonitor.utils.Constants;

/**
 * Created by andrea on 27/04/18.
 */

public class FileCSVManager extends ContextWrapper {

    private static final String FILE_NAME = "log.csv";
    private static final String TAG = FileCSVManager.class.getName();
    private File _file = new File(makeDirs(), FILE_NAME);

    public FileCSVManager(Context contex) {
        super(contex);
    }

    public File makeDirs() {
        File f;
        if (isExternalStorageWritable()) {
            f = new File(Environment.getExternalStorageDirectory(), Constants.SENSOR_DIR_NAME);
        } else {
            f = getFileStreamPath(Constants.SENSOR_DIR_NAME);
        }
        if (!f.exists()) {
            f.mkdirs();
        }
        return f;
    }

    public boolean save(LogResult logResult) {
        try {
            OutputStream outputStream;
            if (!this._file.exists() || this._file.length() == 0) {
                outputStream = new FileOutputStream(this._file);
                try {
                    outputStream.write(logResult.toCSVHeader().getBytes());
                } catch (IOException ex) {
                    ex.printStackTrace();
                    return false;
                }
            }
            outputStream = new FileOutputStream(this._file, true);
            outputStream.write("\n".concat(logResult.toCSVContent()).getBytes());
            outputStream.close();
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public List<LogResult> readCsv() {
        try {
            if (this._file.exists() && this._file.length() > 0) {
                List<LogResult> logResults = new ArrayList<>();
                Scanner scanner = new Scanner(this._file).useDelimiter("\n");
                String header = scanner.next();
                while (scanner.hasNext()) {
                    logResults.add(LogResult.parseFromCsv(header + "\n" + scanner.next()));
                }
                return logResults;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public int getLogsNumber() {
        try {
            if (_file.exists() && _file.length() > 0) {
                Scanner scanner = new Scanner(_file).useDelimiter("\n");
                int count;
                for(count = 0; scanner.hasNext(); count++, scanner.next());
                return count - 1;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean isExternalStorageWritable() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            return true;
        }
        return false;
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
}