package it.petretiandrea.mfmonitor.model.monitor;

import android.content.Context;
import android.content.ContextWrapper;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.model.monitor
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 20/11/17.
 */

public abstract class BaseRegistrableSensor extends ContextWrapper {

    private boolean _sensorRegistered;

    public BaseRegistrableSensor(Context base) {
        super(base);
        _sensorRegistered = false;
    }

    public abstract void startReading();

    public abstract void waitForEnd();

    /**
     * If sensor updates are attached to callback.
     * @return True if the sensor is attached to callback.
     */
    public boolean isSensorStarted()
    {
        return _sensorRegistered;
    }

    protected void setSensorRegistered(boolean sensorRegistered) {
        _sensorRegistered = sensorRegistered;
    }
}
