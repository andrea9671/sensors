package it.petretiandrea.mfmonitor.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by andrea on 27/04/18.
 */

public enum DateFormatter {

    DEFAULT("dd-MM-yyyy HH:mm:ss:SSS", Locale.US),
    FILELOGMANAGER("yyyy_MM_dd_HH_mm_ss_SSS", Locale.US);

    private SimpleDateFormat _format;
    private DateFormatter(String pattern, Locale locale) {
        _format = new SimpleDateFormat(pattern, locale);
    }

    public String format(Date date) {
        return _format.format(date);
    }
    public Date parse(String date) throws ParseException {
        return _format.parse(date);
    }
}
