package it.petretiandrea.mfmonitor.model.monitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import it.petretiandrea.mfmonitor.model.monitor.record.WifiResult;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.model.monitor
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 12/02/18.
 */

public class WifiMonitor extends ContextWrapper {

    private static final String TAG = WifiMonitor.class.getName();
    private static final int SCAN_WIFI_TIMEOUT = 30000; // 30 seconds

    /** Wifi manager **/
    private WifiManager _wifiManager;

    /** Broadcast receiver for scan result **/
    private WifiReceiver _wifiReceiver;

    private Semaphore _semaphore;
    private List<ScanResult> _lastScanResult;

    public WifiMonitor(Context base) {
        super(base);
        _wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        _wifiReceiver = new WifiReceiver();
        _semaphore = new Semaphore(0);
    }

    /**
     * Start scan of WiFi APs, use WakeLock and WiFiLock.
     * @return Task that contains a WiFiResult Object. {@see WiFiResult}
     */
    public Task<WifiResult> startScan()
    {
        return Tasks.call(Executors.newSingleThreadExecutor(), this::scanAccessPoint);
    }

    private WifiResult scanAccessPoint()
    {
        HandlerThread handlerThread = new HandlerThread(TAG);
        WifiManager.WifiLock wifiLock =_wifiManager.createWifiLock(WifiManager.WIFI_MODE_SCAN_ONLY, TAG);

        // wake up WIFI
        wifiLock.acquire();
        Log.d(TAG, "Wifilock acquired");

        // start handler thread
        handlerThread.start();

        // register receiver for scan result
        registerReceiver(_wifiReceiver,
                new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION),
                null,
                new Handler(handlerThread.getLooper()));

        boolean oldOnOffStatus = _wifiManager.isWifiEnabled();
        // turn on wifi
        _wifiManager.setWifiEnabled(true);
        // start scan
        _wifiManager.startScan();
        // Try to acquire permission that is released when a result comes on broadcast receiver
        // there is a timeout if for some reason the scan not terminate.
        try {
            _semaphore.tryAcquire(SCAN_WIFI_TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // stop handler thread
        handlerThread.quit();
        // unregister receiver for stop receiving scan result.
        unregisterReceiver(_wifiReceiver);
        // turn to original state the wifi
        _wifiManager.setWifiEnabled(oldOnOffStatus);
        // release lock for Wifi
        wifiLock.release();

        if(_lastScanResult != null)
            return new WifiResult(_lastScanResult);
        return new WifiResult(new ArrayList<>());
    }

    /**
     * Method to force the turn on WiFi.
     * @param context Context.
     */
    public static void triggerWiFiOn(Context context) {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if(wifiManager != null && !wifiManager.isWifiEnabled())
            wifiManager.setWifiEnabled(true);
    }

    private class WifiReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "SCAN RECEIVED!");
            _lastScanResult = _wifiManager.getScanResults();
            _semaphore.release();
        }
    }
}