package it.petretiandrea.mfmonitor.ui.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

import it.petretiandrea.mfmonitor.R;
import it.petretiandrea.mfmonitor.model.monitor.record.LogResult;


// todo: transform to dialog.
public class LogViewDialog extends DialogFragment {

    private static final String KEY_LOG_RESULT = "log_result";

    private TextView _textMagnetometer;
    private TextView _textMobileData;
    private TextView _textLongitude;
    private TextView _textLatitude;
    private TextView _textIndoor;
    private TextView _textAccuracy;
    private TextView _textProvider;
    private TextView _textAccessPointCount;

    public static LogViewDialog newInstance(LogResult logResult)
    {
        LogViewDialog fragment = new LogViewDialog();
        Bundle args = new Bundle();
        args.putSerializable(KEY_LOG_RESULT, logResult);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.log_dialog, null, false);

        _textMagnetometer = rootView.findViewById(R.id.magnetometer);
        _textMobileData = rootView.findViewById(R.id.mobileData);
        _textLongitude = rootView.findViewById(R.id.longitude);
        _textLatitude = rootView.findViewById(R.id.latitude);
        _textIndoor = rootView.findViewById(R.id.inDoor);
        _textAccuracy = rootView.findViewById(R.id.accuracy);
        _textProvider = rootView.findViewById(R.id.provider);
        _textAccessPointCount = rootView.findViewById(R.id.accessPointCount);

        LogResult logResult = (LogResult) getArguments().getSerializable(KEY_LOG_RESULT);
        setupUi(logResult);
        return new AlertDialog.Builder(getActivity())
                .setView(rootView)
                .setTitle("Log Result")
                .setPositiveButton("Ok", (dialogInterface, i) -> dismiss())
                .create();
    }

    private void setupUi(LogResult logResult)
    {
        _textMagnetometer.setText(String.format(Locale.US, "%f", logResult.getMagnetometer()));
        _textMobileData.setText(logResult.isMobileData() ? "On" : "Off");
        _textLatitude.setText(String.format(Locale.US, "%f", logResult.getLocationResult().getLatitude()));
        _textLongitude.setText(String.format(Locale.US, "%f", logResult.getLocationResult().getLongitude()));
        _textIndoor.setText(logResult.getLocationResult().isInDoor() ? "Yes" : "No");
        _textAccuracy.setText(String.format(Locale.US, "%f", logResult.getLocationResult().getAccuracy()));
        _textProvider.setText(logResult.getLocationResult().getProvider());
        _textAccessPointCount.setText(String.format(Locale.US, "%d", logResult.getWifiResult().getNumberAccessPoint()));
    }
}