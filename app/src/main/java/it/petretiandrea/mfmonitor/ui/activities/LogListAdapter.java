package it.petretiandrea.mfmonitor.ui.activities;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import it.petretiandrea.mfmonitor.R;
import it.petretiandrea.mfmonitor.model.monitor.record.LogResult;

/**
 * Created by Andrea on 19/02/2018.
 */

public class LogListAdapter extends RecyclerView.Adapter<LogListAdapter.ViewHolder> {

    private LogResult[] mDataset;

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTextView;
        ViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.log_name);
        }
    }

    public LogListAdapter(List<LogResult> myDataset)
    {
        mDataset = new LogResult[myDataset.size()];
        myDataset.toArray(mDataset);
    }

    public LogListAdapter(LogResult[] myDataset) {
        mDataset = myDataset;
    }

    @Override
    public LogListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.elem_log_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTextView.setText(mDataset[position].getDate());

    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public LogResult getItem(int position)
    {
        if(position < 0 || position > mDataset.length - 1)
            return null;
        else
            return mDataset[position];
    }
}
