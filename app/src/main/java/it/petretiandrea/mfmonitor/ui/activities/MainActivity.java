package it.petretiandrea.mfmonitor.ui.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import it.petretiandrea.mfmonitor.R;
import it.petretiandrea.mfmonitor.background.DelayedTaskHandlerTask;
import it.petretiandrea.mfmonitor.background.ForegroundSamplingService;
import it.petretiandrea.mfmonitor.background.ServiceMonitorCallback;
import it.petretiandrea.mfmonitor.model.FileCSVManager;
import it.petretiandrea.mfmonitor.utils.Average;
import it.petretiandrea.mfmonitor.utils.ExtendedObjects;
import it.petretiandrea.mfmonitor.utils.SensorMonitorHelper;
import it.petretiandrea.mfmonitor.utils.Utility;

public class MainActivity extends AppCompatActivity implements ServiceConnection, ServiceMonitorCallback {

    private static final String TAG = MainActivity.class.getName();
    private static final int PERMISSIONS = 1000;

    private Intent _backService;

    private TextView _status;
    private ForegroundSamplingService _sampling;
    private TextView _statusDirectSampling;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _status = findViewById(R.id.service_status);
        _statusDirectSampling = findViewById(R.id.info_sampling_service);
        TextView _numberSavedLogs = findViewById(R.id.logs_saved);

        _backService = new Intent(this, ForegroundSamplingService.class);
        FileCSVManager _csvManager = new FileCSVManager(this);

        findViewById(R.id.action_start_service).setOnClickListener(v -> {
            // Start button check if location
            if(checkTypeGeoLocalization()) {
                SensorMonitorHelper.getInstance().schedule(MainActivity.this, 5000);
                setServiceStatus();
            }
        });

        findViewById(R.id.action_stop_service).setOnClickListener(v -> {
            SensorMonitorHelper.getInstance().removeSchedule(MainActivity.this);
            setServiceStatus();
        });

        findViewById(R.id.action_start_direct_sampling).setOnClickListener(v -> {
            DelayedTaskHandlerTask delayedTaskHandlerTask = new DelayedTaskHandlerTask(20000);
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.direct_sampling))
                    .setMessage(String.format(Locale.US, getString(R.string.countdown_message), 20000))
                    .setCancelable(true)
                    .setOnCancelListener(dialog1 -> { delayedTaskHandlerTask.cancel(); dialog1.dismiss(); })
                    .setNegativeButton(getString(R.string.cancel), (dialog12, which) -> dialog12.cancel())
                    .create();

            delayedTaskHandlerTask.setObserver(new DelayedTaskHandlerTask.Observer() {
                @Override
                public void onStartCountdown() {
                    if(dialog != null)
                        dialog.show();
                }

                @Override
                public void onTick(long remeaningTime) {
                    if(dialog != null)
                        dialog.setMessage(String.format(Locale.US, getString(R.string.countdown_message), (remeaningTime / 1000)));
                }

                @Override
                public void onCountdownEnd() {
                    if(dialog != null)
                        dialog.dismiss();
                    if(ExtendedObjects.nonNull(_sampling) && !_sampling.isRunning()) {
                        SensorMonitorHelper.getInstance().directSampling(getApplicationContext());
                    }
                }
            });
            delayedTaskHandlerTask.start();
        });

        findViewById(R.id.action_view_logs).setOnClickListener(v -> {
            Intent i = new Intent(MainActivity.this, LogViewer.class);
            startActivity(i);
        });

        _numberSavedLogs.setText(String.format(Locale.US, "%d", _csvManager.getLogsNumber()));
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindService(_backService, this, BIND_AUTO_CREATE);
        Log.d(TAG, "onStart() called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkAndAskPermission();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(this);
        Log.d(TAG, "onStop() called");
    }


    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Log.d(TAG, "Service Bind!");
        _sampling = ((ForegroundSamplingService.LocalBinder)iBinder).getService();
        _sampling.setMonitorListener(this);
        setServiceStatus();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        Log.d(TAG, "Service disconnected!");
    }

    @Override
    public void onStartMonitoring() {
        runOnUiThread(this::setServiceStatus);
    }

    @Override
    public void onStopMonitoring() {
        runOnUiThread(this::setServiceStatus);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*if(item.getItemId() == R.id.settings)
        {
            // startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }*/
        if(item.getItemId() == R.id.go_to_map) {
            Uri uri = Uri.parse("https://mfmonitor-97879.firebaseapp.com/");
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            if(i.resolveActivity(getPackageManager()) != null)
                startActivity(i);
            else
                Toast.makeText(this, "No Browser Available!", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean checkTypeGeoLocalization() {
        boolean gpsNet[] = Utility.isLocationServiceEnabled(this);
        if(!gpsNet[0] && gpsNet[1]) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.geolocalization))
                    .setMessage(getString(R.string.warn_location_network))
                    .setPositiveButton(getString(R.string.ok), (dialog1, which) -> dialog1.dismiss())
                    .setCancelable(true)
                    .create()
                    .show();
        } else if(!gpsNet[0] && !gpsNet[1]) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.geolocalization))
                    .setMessage(getString(R.string.error_geolocalization_off))
                    .setPositiveButton(getString(R.string.action_enable_geolocalization), (dialog1, which) -> {
                        Intent settings = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(settings);
                    })
                    .setNegativeButton(getString(R.string.exit), (dialog12, which) -> finish())
                    .setCancelable(false)
                    .create()
                    .show();
            return false;
        }
        return true;
    }

    private void checkAndAskPermission()
    {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case PERMISSIONS:
                if(grantResults.length > 1) {
                    boolean location = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean externalStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if(!location || !externalStorage)
                    {
                        Toast.makeText(this, "Ho bisogno dei permessi per poter monitorare :(", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
                break;
        }
    }

    private void setServiceStatus()
    {
        if(_sampling != null)
        {
            if(_sampling.isRunning()) {
                _status.setTextColor(Color.GREEN);
                _status.setText(getString(R.string.running));
            }
            else if(SensorMonitorHelper.getInstance().isScheduled(MainActivity.this))
            {
                _status.setTextColor(Color.rgb(255,140,0));
                _status.setText(getString(R.string.scheduled));
            }
            else
            {
                _status.setTextColor(Color.RED);
                _status.setText(getString(R.string.not_running));
            }
        }
    }
}
