package it.petretiandrea.mfmonitor.ui.activities;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.annimon.stream.Stream;

import java.text.ParseException;

import it.petretiandrea.mfmonitor.R;
import it.petretiandrea.mfmonitor.RecyclerTouchListener;
import it.petretiandrea.mfmonitor.model.FileCSVManager;
import it.petretiandrea.mfmonitor.model.monitor.record.LogResult;
import it.petretiandrea.mfmonitor.utils.Utility;

public class LogViewer extends AppCompatActivity {

    private static final String TAG = LogViewer.class.getName();

    private LogListAdapter _logListAdapter;
    private FileCSVManager _fileCsvManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setContentView(R.layout.activity_log_viewer);
        ((TextView)findViewById(R.id.txt_info_timezone)).setText(String.format(getString(R.string.timezone_info), Utility.getLocalOffsetTimeZone()));

        RecyclerView _recyclerView = findViewById(R.id.recyclerView);
        _recyclerView.setLayoutManager(new LinearLayoutManager(this));

        _fileCsvManager = new FileCSVManager(this);
        _logListAdapter = new LogListAdapter(Stream.of(_fileCsvManager.readCsv()).sorted((logResult, t1) -> {
            try {
                return Utility.parseDateTime(t1.getDate()).compareTo(Utility.parseDateTime(logResult.getDate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return 0;
        }).toList());

        _recyclerView.setAdapter(_logListAdapter);
        _recyclerView.addOnItemTouchListener(new RecyclerTouchListener() {
            @Override
            public void onClick(View view, int position) {
                LogViewer.this.showDialog(LogViewer.this._logListAdapter.getItem(position));
            }
        });
    }

    private void showDialog(LogResult logResult) {
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.addToBackStack(null);

        DialogFragment newFragment = LogViewDialog.newInstance(logResult);
        newFragment.show(ft, "logdialog");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}