package it.petretiandrea.mfmonitor.utils;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.utils
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 20/11/17.
 */

public class Constants {

    public static int ALARM_SCHEDULE_ID = 130596;
    public static int ALARM_SCHEDULE_ID_DIRECT_SAMPLING = 1305907;
    public static int FOREGROUND_SERVICE_ID = 1;
    public static int DEFAULT_ERR_TEMP = -1000;

    public static String FOREGROUND_SERVICE_START = "foregroundservicestart";
    public static String FOREGROUND_SERVICE_STOP = "foregroundservicestop";
    public static String FOREGROUND_SAMPLING_TIME = "foregroundsamplingtime";
    public static String FOREGROUND_RECORD_TIME = "foregroundrecordtime";
    public static String FOREGROUND_SENSOR_LIST = "foregroundsensorlist";

    public static String SHARED_PREF_NAME = "sharedprefssensors";
    public static String KEY_PREF_ALARM_MONITOR = "alarm_monitor_scheduled";

    public static String SENSOR_DIR_NAME = "MFMonitorLogs";
    public static String NEXT_SCHEDULE_INTERVAL = "next_schedule_interval";
}
