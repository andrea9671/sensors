package it.petretiandrea.mfmonitor.utils;

import java.util.Objects;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.utils
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 15/02/18.
 */

public class Average {

    public static <T extends Number> Double average(T[] values)
    {
        if(values != null && values.length > 0)
        {
            Double sum = 0d;
            for (T value : values) sum +=  (value != null ) ? value.doubleValue() : 0d;
            sum = sum / values.length;
            return sum;
        }
        return 0d;
    }

    /**
     * Calculate the average of every n values in the array.
     * @param values Original Array
     * @param <T>
     * @return
     */
    public static <T extends Number> Double[] averageEvery(T[] values, int n)
    {
        if(values.length > 0 && n > 0) {
            Double sum = 0d;
            int length = (values.length) / n;
            int module = (values.length) % n;
            Double[] av = new Double[length + 1];
            int i;

            // Average for chunck of dimension n
            for (i = 0; i < length; i++) {
                for (int j = (n * i); j < (n * (i + 1)); j++)
                    sum += values[j].doubleValue();
                av[i] = (sum / n);
                sum = 0d;
            }

            if(module > 0) {
                // Average for chunck of dimension < n
                for (int j = (n * i); j < values.length; j++)
                    sum += values[j].doubleValue();
                av[i] = (sum / module);
            }

            return av;
        }
        return new Double[]{};
    }
}
