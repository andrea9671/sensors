package it.petretiandrea.mfmonitor.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;

import it.petretiandrea.mfmonitor.background.ForegroundSamplingService;
import it.petretiandrea.mfmonitor.broadcast.AlarmBroadcastReceiver;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.utils
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 25/11/17.
 */

public class SensorMonitorHelper {

    private static SensorMonitorHelper _instance;

    public static SensorMonitorHelper getInstance()
    {
        if(_instance == null)
            _instance = new SensorMonitorHelper();
        return  _instance;
    }

    private SensorMonitorHelper() { }

    public void delayedSampling(Context context, long delayMs) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if(alarmManager != null) {
            // Broadcast intent with sampling time
            Intent broadcast = new Intent(context, AlarmBroadcastReceiver.class);
            broadcast.putExtra(Constants.FOREGROUND_SERVICE_START, Constants.FOREGROUND_SERVICE_START);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                    Constants.ALARM_SCHEDULE_ID_DIRECT_SAMPLING,
                    broadcast,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.cancel(pendingIntent);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, (System.currentTimeMillis() + delayMs), pendingIntent);
        }
    }

    public boolean isDelayedSampleStarted(Context context)
    {
        return checkPendingIntentExist(context, Constants.ALARM_SCHEDULE_ID_DIRECT_SAMPLING);
    }

    public void directSampling(Context context) {
        Intent background = new Intent(context, ForegroundSamplingService.class);
        background.putExtra(Constants.FOREGROUND_SERVICE_START, Constants.FOREGROUND_SERVICE_START);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            context.startForegroundService(background);
        else
            context.startService(background);
    }



    // todo: inviare monitor list
    public void schedule(Context context, int delayMs)
    {
        if(delayMs >= 0) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            // Recupero dati dalle preferenze
            PreferencesHelper preferencesHelper = new PreferencesHelper(context);
            int nextScheduleInterval = 5 * (1000 * 60); // default 5 minutes;
            //int nextScheduleInterval = 2 * (1000 * 60);


            if (alarmManager != null) {
                // Broadcast intent with sampling time
                Intent broadcast = new Intent(context, AlarmBroadcastReceiver.class);
                broadcast.putExtra(Constants.FOREGROUND_SERVICE_START, Constants.FOREGROUND_SERVICE_START);
                broadcast.putExtra(Constants.NEXT_SCHEDULE_INTERVAL, nextScheduleInterval);
                //broadcast.putExtra(Constants.FOREGROUND_RECORD_TIME, samplingRecordTime);
                //broadcast.putExtra(Constants.FOREGROUND_SAMPLING_TIME, samplingPeriod);
                //broadcast.putExtra(Constants.FOREGROUND_SENSOR_LIST, sensorsList);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                        Constants.ALARM_SCHEDULE_ID,
                        broadcast,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                // Cancel all pending schedule allarm
                alarmManager.cancel(pendingIntent);
                alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo((System.currentTimeMillis() + delayMs), pendingIntent), pendingIntent);
                // alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, (System.currentTimeMillis() + delayMs), nextScheduleInterval, pendingIntent);
                preferencesHelper.setAlarmMonitorScheduled(true);
            }
        }
    }

    public void removeSchedule(Context context)
    {
        PreferencesHelper preferencesHelper = new PreferencesHelper(context);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if(alarmManager != null) {
            Intent broadcast = new Intent(context, AlarmBroadcastReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                    Constants.ALARM_SCHEDULE_ID,
                    broadcast,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            // Cancel all pending schedule allarm
            alarmManager.cancel(pendingIntent);
            pendingIntent.cancel();
            preferencesHelper.setAlarmMonitorScheduled(false);
        }
    }

    public boolean isScheduled(Context context)
    {
        return checkPendingIntentExist(context, Constants.ALARM_SCHEDULE_ID);
    }


    private boolean checkPendingIntentExist(Context context, int pendingIntentId) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if(alarmManager != null)
        {
            Intent broadcast = new Intent(context, AlarmBroadcastReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                    pendingIntentId,
                    broadcast,
                    PendingIntent.FLAG_NO_CREATE);
            return  pendingIntent != null;
        }
        return false;
    }
}
