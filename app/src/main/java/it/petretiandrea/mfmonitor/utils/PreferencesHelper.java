package it.petretiandrea.mfmonitor.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;

/**
 * Project Sensors
 * Package it.petretiandrea.sensors.utils
 * Created by Petreti Andrea petretiandrea@gmail.com.
 * Created at 21/11/17.
 */

// TODO: Finire implmentazione
public class PreferencesHelper extends ContextWrapper {

    private SharedPreferences _sharedPreferences;

    public PreferencesHelper(Context base) {
        super(base);
        _sharedPreferences = getApplicationContext().getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Set if the allarm manager have pending intent for monitor scheduled.
     * @param scheduled If is scheduled
     */
    public void setAlarmMonitorScheduled(boolean scheduled)
    {
        _sharedPreferences.edit().putBoolean(Constants.KEY_PREF_ALARM_MONITOR, scheduled).apply();
    }

    /**
     * @return If the allarm manager have pending intent for monitor scheduled.
     */
    public boolean isAlarmMonitorScheduled()
    {
        return _sharedPreferences.getBoolean(Constants.KEY_PREF_ALARM_MONITOR, false);
    }

    public void setUUID(String uuid)
    {
        _sharedPreferences.edit().putString("uuid", uuid).apply();
    }

    public String getUUID()
    {
        return _sharedPreferences.getString("uuid", "");
    }
}
