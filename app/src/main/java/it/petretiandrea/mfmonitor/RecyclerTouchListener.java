package it.petretiandrea.mfmonitor;

/**
 * Created by andrea on 21/02/18.
 */

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import static android.support.v7.widget.RecyclerView.NO_POSITION;

/**
 * Project smartjar-android
 * Created by Petreti Andrea on 29/09/17.
 */

public abstract class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private static final int SWIPE_MAX_OFF_PATH = 250;

    private GestureDetectorCompat mGestureDetector;
    private Context mContext;
    private RecyclerView mRecyclerView;

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        mRecyclerView = rv;
        mContext = rv.getContext();
        instantiateGestureDetector();
        mGestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        // do nothing
    }


    private void instantiateGestureDetector()
    {
        if(mGestureDetector == null)
        {
            mGestureDetector = new GestureDetectorCompat(mContext, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    View child = mRecyclerView.findChildViewUnder(e.getX(), e.getY());
                    int childPosition = mRecyclerView.getChildAdapterPosition(child);
                    if(childPosition != NO_POSITION)
                        RecyclerTouchListener.this.onLongClick(child, childPosition);

                }

                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    // Find the item view that was swiped based on the coordinates
                    View child = mRecyclerView.findChildViewUnder(e.getX(), e.getY());
                    int childPosition = mRecyclerView.getChildAdapterPosition(child);
                    if(childPosition != NO_POSITION)
                        onClick(child, childPosition);
                    return false;
                }

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

                    try {
                        if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) {
                            return false;
                        }

                        // Find the item view that was swiped based on the coordinates
                        View child = mRecyclerView.findChildViewUnder(e1.getX(), e1.getY());
                        int childPosition = mRecyclerView.getChildAdapterPosition(child);

                        if(childPosition != NO_POSITION)
                        {
                            // right to left swipe
                            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

                                if (child != null) {
                                    onLeftSwipe(child, childPosition);
                                }

                            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                                if (child != null) {
                                    onRightSwipe(child, childPosition);
                                }
                            }
                        }
                    } catch (Exception e) {
                        // nothing
                    }

                    return false;
                }
            });
        }
    }

    public void onLeftSwipe(View view, int position){}
    public void onRightSwipe(View view, int position){}
    public abstract void onClick(View view, int position);
    public void onLongClick(View view, int position){}
}
